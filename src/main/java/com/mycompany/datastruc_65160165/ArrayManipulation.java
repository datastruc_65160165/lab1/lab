/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.datastruc_65160165;

/**
 *
 * @author 65160
 */
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
import java.util.Arrays;

public class ArrayManipulation {

    public static void main(String[] args) {
        //Inside the "ArrayManipulation" class, declare and initialize the following arrays:
        //An integer array named "numbers" with the values {5, 8, 3, 2, 7}.
        int[] numbers = {5, 8, 3, 2, 7};

        //A string array named "names" with the values {"Alice", "Bob", "Charlie", "David"}.
        String[] names = {"Alice", "Bob", "Charlie", "David"};

        //A double array named "values" with a length of 4 (do not initialize the elements yet).
        double[] values = new double[4];

        //Print the elements of the "numbers" array using a for loop.
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        //Print the elements of the "names" array using a for-each loop.
        System.out.println();
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }

        //Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 5.3;
        values[1] = 8.7;
        values[2] = 3.2;
        values[3] = 2.7;

        //Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        System.out.printf("\nThe sum of all elements in the numbers array is " + sum);

        //Find and print the maximum value in the "values" array.
        double max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            } else {
                continue;
            }
        }
        System.out.printf("\nThe maximum value in the values array is " + max);

        //Create a new string array named "reversedNames" with the same length as the "names" array.
        String[] reversedNames = new String[4];

        //Fill the "reversedNames" array with the elements of the "names" array in reverse order.
        for (int i = 0; i < names.length; i++) {
            String j = names[i];
            reversedNames[i] = names[names.length - i - 1];
            reversedNames[names.length - i - 1] = j;
        }
        System.out.println();

        //Print the elements of the "reversedNames" array.
        for (int i = 0; i < reversedNames.length; i++) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println();

        //BONUS: Sort the "numbers" array in ascending order using any sorting algorithm of your choice.
        Arrays.sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
    }
}
